package be.vdab.ExtraH5.oef4;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int num1 = askNumber();
        int num2 = askNumber();
        int sum = num1 + num2;
        System.out.println("The sum of both numbers = " + sum);

        int multiply = multiplicationBetweenNumbers(num1, num2);
        System.out.println("The multiplication of all the between numbers is " + multiply);
    }

    public static int askNumber() {
        System.out.println("Type in a number: ");

        int num = 0;
        while(true) {
            try {
                num = scanner.nextInt();
                break;
            } catch(RuntimeException e) {
                System.out.println("Enter a valid number: ");
            } finally {
                scanner.nextLine();
            }
        }

        return num;
    }

    public static int multiplicationBetweenNumbers(int num1, int num2) {

        int min = num1 < num2 ? num1 : num2;
        int max = num1 < num2 ? num2 : num1;
        int[] multiplyArray = new int[max - min + 1];
        int multiply = 1;

        for(int i = 0; i < multiplyArray.length; i++) {
            multiplyArray[i] = min++;
        }

        for(int i = 1; i < multiplyArray.length -1; i++) {
            multiply *= multiplyArray[i];
        }

        return multiply;
    }
}